﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ModelClassroom
{
    [TestClass]
    public class TestStudent
    {
        [TestMethod]
        public void TestStudentFirstname()
        {
            Student student = new Student("Joe", "Dalton");
            Assert.AreEqual("Joe", student.Firstname);

            student.Firstname = "John";
            Assert.AreEqual("John", student.Firstname);
        }

        [TestMethod]
        public void TestStudentLastname()
        {
            Student student = new Student("Joe", "Doe");
            Assert.AreEqual("DOE", student.Lastname);

            student.Lastname = "Dalton";
            Assert.AreEqual("DALTON", student.Lastname);
        }
    }
}
