﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ModelClassroom
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class TestStudentLoader
    {
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test_CSV_FileExsits()
        {
            //Verification si fichier existe
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test_CSV_FileIsEmpty()
        {
            SchoolClass theClass = new SchoolClass();
            string pathToFile = "..//..//..//Model//students//emptyfile.csv";
            //Assert.ThrowsException<Exception>(() => theClass.CheckIfFileIsEmpty(pathToFile));
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test_CSV_DataAttributsExists()
        {
            //Verifer s'il n y a pas de doublons dans les données
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test_CSV_SudentCouldntLoad()
        {
            //Verifer si tout les étudiants sont importer
        }


    }
}
