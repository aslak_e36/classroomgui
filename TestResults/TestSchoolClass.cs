﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClassroomNamespace
{
    [TestClass]
    public class TestClassroom
    {
        [TestMethod]
        public void TestOneStudent()
        {

            SchoolClass theClass = new SchoolClass();

            Student student1 = new Student();

            theClass.Students.Add(student1);

            Assert.IsTrue(theClass.Students.Contains(student1));
        }

        [TestMethod]
        public void TestTwoStudents()
        {
            SchoolClass theClass = new SchoolClass();

            Student student1 = new Student();
            Student student2 = new Student();

            theClass.Students.Add(student1);
            theClass.Students.Add(student2);

            Assert.IsTrue(theClass.Students.Contains(student1));
            Assert.IsTrue(theClass.Students.Contains(student2));
        }

        [TestMethod]
        public void TestNameCorrect()
        {
            SchoolClass theClass = new SchoolClass();
            theClass.Name = "SI-C3b";

            Assert.AreEqual("SI-C3b", theClass.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestNameIncorrect()
        {
            SchoolClass theClass = new SchoolClass();
            theClass.Name = "YOUH";
        }

        [TestMethod]
        public void TestGetRandomStudent()
        {
            SchoolClass theClass = new SchoolClass();
            int totalStudentsCount = 20;
            int randomPickedStudentsCount = 5;
            int minAllowedDuplicates = 3;

            // Add dummy students  
            for (int i = 0; i < totalStudentsCount; i++)
            {
                theClass.Students.Add(new Student($"{i}", ""));
            }

            // Getting 2 random students out of 10 may give twice the same,
            // but getting 3 out of 10 should not give 3 times the same.
            List<Student> randomStudents = new List<Student>();
            for (int i = 0; i < randomPickedStudentsCount; i++)
            {
                randomStudents.Add(theClass.GetRandomStudent());
            }

            // Remove duplicates in the random students and check their counts
            Assert.IsTrue(randomStudents.Distinct().ToList().Count > minAllowedDuplicates, $"Getting {randomPickedStudentsCount} students out of {totalStudentsCount} permits {minAllowedDuplicates} or more duplicates, got less!");

        }
    }
}
