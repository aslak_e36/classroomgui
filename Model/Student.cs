﻿using System.Collections.Generic;

namespace ModelClassroom
{
    /// <summary>
    /// The class for students
    /// </summary>
    public class Student
    {

        #region private attributes
        private string name = "";
        private string firstname = "";
        private string birthday = "";
        private string picture = "";
        private List<Student> students = null;
        #endregion


        #region constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="name"></param>
        public Student(string firstname, string name, string birthday, string picture)
        {
            this.name = name;
            this.firstname = firstname;
            this.birthday = birthday;
            this.picture = picture;
        }

        /// <summary>
        /// This is the contructor for a new student 
        /// </summary>
        /// <param name="firstname">The firstname of the student</param>
        /// <param name="name">The lastname of the student</param>
        public Student(string firstname, string name)
        {
            this.name = name;
            this.firstname = firstname;
        }

        /// <summary>
        /// Construcotr for a student without any parameter
        /// </summary>
        public Student()
        {
            this.students = new List<Student>();
        }
        #endregion


        #region methodes
        
        #endregion


        #region accessors and mutators

        /// <summary>
        /// Accessor for Firstname of students
        /// </summary>
        public string Firstname
        {
            get { return this.firstname.Substring(0, 1).ToUpper() + this.firstname.Substring(1); }
            set { this.firstname = value; }
        }


        /// <summary>
        /// Accessor for Lastname of students
        /// </summary>
        public string Lastname
        {
            get { return this.name.ToUpper(); }
            set { this.name = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Birthday
        {
            get { return this.birthday; }
            set { this.birthday = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Picture
        {
            get { return this.picture; }
            set { this.picture = value; }
        }
        #endregion

    }
}
