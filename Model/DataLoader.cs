﻿using System.Collections.Generic;
using System.IO;

namespace ModelClassroom
{
    /// <summary>
    /// In this class we gonna load all students of a classroom from a csv
    /// </summary>
    public class DataLoader
    {
        #region Private attribute
        #endregion

        #region Contructor
        #endregion

        #region Methods
        /// <summary>
        /// This method allows to load a CSV file and return its values in string
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public List<string> LoadCSV(string path)
        {
            List<string> items = new List<string>();
            using (var reader = new StreamReader(@path))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    items.Add(line);
                }
            }
            return items;
        }
        #endregion

        #region Getter;Setter
        #endregion
    }
}
