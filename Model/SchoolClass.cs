﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace ModelClassroom
{
    /// <summary>
    /// This is the class for School classes
    /// </summary>
    public class SchoolClass
    {
        #region private attributes
        private string name = "";
        private Random rand = null;
        #endregion


        #region constructor
        /// <summary>
        /// Constructor for a class
        /// </summary>
        public SchoolClass()
        {
            this.Students = new List<Student>();
            this.rand = new Random();
        }

        public SchoolClass(string pathToFile, List<string> dataFromFile)
        {
            this.Students = new List<Student>();
            CheckIfAllAtributesExists(dataFromFile);
            CheckForDuplicate(dataFromFile);
        }

        #endregion


        #region methodes
        /// <summary>
        /// Method tha gets a random student
        /// </summary>
        /// <returns></returns>
        public Student GetRandomStudent()
        {
            return Students[this.rand.Next(0, Students.Count - 1)];
        }

        /// <summary>
        /// This method checks if there's any duplicate into the list that came from the file
        /// </summary>
        /// <param name="studentFromFile">List of string that contains students' data</param>
        public void CheckForDuplicate(List<string> studentFromFile)
        {
            try
            {
                List<string> duplicate = new List<string>();
                foreach (string item in studentFromFile)
                {
                    if (duplicate.Contains(item))
                    {
                        throw new Exception("Un élève à double a été détecté, veuillez revoir votre fichier de départ");
                    }
                    else
                    {
                        duplicate.Add(item);
                    }
                }
            }
            catch (Exception e)
            {
                var error = MessageBox.Show("Erreur : " + e.Message);
            }
        }

        /// <summary>
        /// This method checks if the file exists
        /// </summary>
        /// <param name="pathToFile">Path to the file</param>
        public void CheckIfFileExists(string pathToFile)
        {
            try
            {
                if (!File.Exists(pathToFile))
                {
                    throw new Exception("Le fichier demandé n'existe pas");
                }
            }
            catch (Exception e)
            {

                var error = MessageBox.Show("Erreur : " + e.Message);
            }
        }

        /// <summary>
        /// This method checks if the choosen file is empty
        /// </summary>
        /// <param name="pathToFile">Path to the file</param>
        public void CheckIfFileIsEmpty(string pathToFile)
        {
            try
            {
                if (new FileInfo(pathToFile).Length == 0)
                {
                    throw new Exception("Le fichier semble être vide. Veuillez le vérifier");
                }
            }
            catch (Exception e)
            {
                var error = MessageBox.Show("Erreur : " + e.Message); ;
            }
        }

        /// <summary>
        /// This method will check if all attributes are filled
        /// </summary>
        /// <param name="studentFromFile">List of string that contains students' data</param>
        public void CheckIfAllAtributesExists(List<string> studentFromFile)
        {
            try
            {
                foreach (string item in studentFromFile)
                {
                    var values = item.Split(';');
                    string firstname = values[0];
                    string name = values[1];
                    string birthday = values[2];
                    string picture = values[3];
                    if (firstname == null || firstname == "" || name == null || name == "" || birthday == null || birthday == "" || picture == null || picture == "")
                    {
                        throw new Exception("Un attribut manquant sur un élève, veuillez revoir votre fichier de données");
                    }
                }

            }
            catch (Exception e)
            {
                var error = MessageBox.Show("Erreur : " + e.Message);
            }
        }
        #endregion


        #region accessors and mutators
        /// <summary>
        /// A list of object student
        /// </summary>
        public List<Student> Students { get; set; } = null;

        /// <summary>
        /// Accessor for the name of the class
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set
            {
                if (value.Contains("-"))
                {
                    this.name = value;
                }
                else
                {
                    throw new Exception();
                }
            }
        }
        #endregion
    }
}
