﻿namespace FormsClassroom
{
    partial class FrmClassroom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpStudents = new System.Windows.Forms.GroupBox();
            this.lstStudents = new System.Windows.Forms.ListBox();
            this.lstClass = new System.Windows.Forms.ListBox();
            this.grpClassroom = new System.Windows.Forms.GroupBox();
            this.grpStudents.SuspendLayout();
            this.grpClassroom.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpStudents
            // 
            this.grpStudents.Controls.Add(this.lstStudents);
            this.grpStudents.Location = new System.Drawing.Point(13, 85);
            this.grpStudents.Name = "grpStudents";
            this.grpStudents.Size = new System.Drawing.Size(296, 353);
            this.grpStudents.TabIndex = 1;
            this.grpStudents.TabStop = false;
            this.grpStudents.Text = "Students";
            // 
            // lstStudents
            // 
            this.lstStudents.FormattingEnabled = true;
            this.lstStudents.Location = new System.Drawing.Point(7, 20);
            this.lstStudents.Name = "lstStudents";
            this.lstStudents.Size = new System.Drawing.Size(283, 316);
            this.lstStudents.TabIndex = 0;
            this.lstStudents.SelectedIndexChanged += new System.EventHandler(this.LstStudents_SelectedIndexChanged);
            // 
            // lstClass
            // 
            this.lstClass.FormattingEnabled = true;
            this.lstClass.Location = new System.Drawing.Point(6, 30);
            this.lstClass.Name = "lstClass";
            this.lstClass.Size = new System.Drawing.Size(284, 17);
            this.lstClass.TabIndex = 2;
            this.lstClass.SelectedIndexChanged += new System.EventHandler(this.LstClass_SelectedIndexChanged);
            // 
            // grpClassroom
            // 
            this.grpClassroom.Controls.Add(this.lstClass);
            this.grpClassroom.Location = new System.Drawing.Point(13, 13);
            this.grpClassroom.Name = "grpClassroom";
            this.grpClassroom.Size = new System.Drawing.Size(296, 66);
            this.grpClassroom.TabIndex = 3;
            this.grpClassroom.TabStop = false;
            this.grpClassroom.Text = "Classroom";
            // 
            // FrmClassroom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 450);
            this.Controls.Add(this.grpClassroom);
            this.Controls.Add(this.grpStudents);
            this.Name = "FrmClassroom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Classroom";
            this.Load += new System.EventHandler(this.FrmClassroom_Load);
            this.grpStudents.ResumeLayout(false);
            this.grpClassroom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpStudents;
        private System.Windows.Forms.ListBox lstClass;
        private System.Windows.Forms.GroupBox grpClassroom;
        private System.Windows.Forms.ListBox lstStudents;
    }
}