﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ModelClassroom;

namespace FormsClassroom
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FrmClassroom : Form
    {
        private SchoolClass classroom = null;
        private Student student = null;
        private DataLoader dataLoader = null;
        private List<string> students;

        /// <summary>
        /// 
        /// </summary>
        public FrmClassroom()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmClassroom_Load(object sender, EventArgs e)
        {
            classroom = new SchoolClass();
            classroom.Name = "SI-C3a";
            lstClass.Items.Add(classroom.Name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LstClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataLoader = new DataLoader();
            classroom = new SchoolClass();
            string pathToFile = "..\\..\\..\\Model\\students\\allStudents.csv";
            string name;
            string firstname;
            string birthday;
            string pathToPic;
            string checkListStudent;

            classroom.CheckIfFileExists(pathToFile);
            classroom.CheckIfFileIsEmpty(pathToFile);
            students = dataLoader.LoadCSV(pathToFile);
            classroom = new SchoolClass(pathToFile, students);

            foreach (string item in students)
            {
                var student = item.Split(';');
                firstname = student[0].Substring(0, 1).ToUpper() + student[0].Substring(1);
                name = student[1].ToUpper();
                birthday = student[2];
                pathToPic = student[3];
                checkListStudent = firstname + " " + name;
                this.student = new Student(firstname, name, birthday, pathToPic);
                classroom.Students.Add(this.student);
                if (!lstStudents.Items.Contains(checkListStudent))
                {
                    lstStudents.Items.Add(firstname + " " + name);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LstStudents_SelectedIndexChanged(object sender, EventArgs e)
        {
            string studentFullName = lstStudents.GetItemText(lstStudents.SelectedItem);
            string studentFirstname = "";
            string studentLastname = "";
            string studentBirthday = "";
            string studentPicture = "";

            foreach (Student student in classroom.Students)
            {
                if (student.Firstname.ToLower() + " " + student.Lastname.ToLower() == studentFullName.ToLower())
                {
                    studentFirstname = student.Firstname;
                    studentLastname = student.Lastname;
                    studentBirthday = student.Birthday;
                    studentPicture = student.Picture;
                }
            }
            FrmStudents frmStudent = new FrmStudents(studentFirstname, studentLastname, studentBirthday, studentPicture);
            DialogResult dialogResult = frmStudent.ShowDialog();
        }
    }
}
