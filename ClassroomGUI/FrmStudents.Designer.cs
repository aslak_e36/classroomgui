﻿namespace FormsClassroom
{
    partial class FrmStudents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStudents));
            this.grpStudentName = new System.Windows.Forms.GroupBox();
            this.picStudent = new System.Windows.Forms.PictureBox();
            this.grpPersonalData = new System.Windows.Forms.GroupBox();
            this.txtBirthday = new System.Windows.Forms.TextBox();
            this.lblBirthday = new System.Windows.Forms.Label();
            this.txtFirstname = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblFirstname = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.grpActions = new System.Windows.Forms.GroupBox();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdSave = new System.Windows.Forms.Button();
            this.cmdEdit = new System.Windows.Forms.Button();
            this.grpStudentName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStudent)).BeginInit();
            this.grpPersonalData.SuspendLayout();
            this.grpActions.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpStudentName
            // 
            this.grpStudentName.Controls.Add(this.picStudent);
            this.grpStudentName.Controls.Add(this.grpPersonalData);
            this.grpStudentName.Location = new System.Drawing.Point(13, 13);
            this.grpStudentName.Name = "grpStudentName";
            this.grpStudentName.Size = new System.Drawing.Size(529, 277);
            this.grpStudentName.TabIndex = 0;
            this.grpStudentName.TabStop = false;
            this.grpStudentName.Text = "Student\'s name";
            // 
            // picStudent
            // 
            this.picStudent.Image = ((System.Drawing.Image)(resources.GetObject("picStudent.Image")));
            this.picStudent.ImageLocation = "";
            this.picStudent.InitialImage = null;
            this.picStudent.Location = new System.Drawing.Point(7, 20);
            this.picStudent.Name = "picStudent";
            this.picStudent.Size = new System.Drawing.Size(210, 251);
            this.picStudent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picStudent.TabIndex = 2;
            this.picStudent.TabStop = false;
            // 
            // grpPersonalData
            // 
            this.grpPersonalData.Controls.Add(this.txtBirthday);
            this.grpPersonalData.Controls.Add(this.lblBirthday);
            this.grpPersonalData.Controls.Add(this.txtFirstname);
            this.grpPersonalData.Controls.Add(this.txtName);
            this.grpPersonalData.Controls.Add(this.lblFirstname);
            this.grpPersonalData.Controls.Add(this.lblName);
            this.grpPersonalData.Location = new System.Drawing.Point(223, 19);
            this.grpPersonalData.Name = "grpPersonalData";
            this.grpPersonalData.Size = new System.Drawing.Size(300, 252);
            this.grpPersonalData.TabIndex = 1;
            this.grpPersonalData.TabStop = false;
            this.grpPersonalData.Text = "Personal data";
            // 
            // txtBirthday
            // 
            this.txtBirthday.Enabled = false;
            this.txtBirthday.Location = new System.Drawing.Point(102, 109);
            this.txtBirthday.Name = "txtBirthday";
            this.txtBirthday.Size = new System.Drawing.Size(192, 20);
            this.txtBirthday.TabIndex = 5;
            // 
            // lblBirthday
            // 
            this.lblBirthday.AutoSize = true;
            this.lblBirthday.Location = new System.Drawing.Point(9, 109);
            this.lblBirthday.Name = "lblBirthday";
            this.lblBirthday.Size = new System.Drawing.Size(45, 13);
            this.lblBirthday.TabIndex = 4;
            this.lblBirthday.Text = "Birthday";
            // 
            // txtFirstname
            // 
            this.txtFirstname.Enabled = false;
            this.txtFirstname.Location = new System.Drawing.Point(102, 68);
            this.txtFirstname.Name = "txtFirstname";
            this.txtFirstname.Size = new System.Drawing.Size(192, 20);
            this.txtFirstname.TabIndex = 3;
            // 
            // txtName
            // 
            this.txtName.Enabled = false;
            this.txtName.Location = new System.Drawing.Point(102, 35);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(192, 20);
            this.txtName.TabIndex = 2;
            // 
            // lblFirstname
            // 
            this.lblFirstname.AutoSize = true;
            this.lblFirstname.Location = new System.Drawing.Point(6, 71);
            this.lblFirstname.Name = "lblFirstname";
            this.lblFirstname.Size = new System.Drawing.Size(52, 13);
            this.lblFirstname.TabIndex = 1;
            this.lblFirstname.Text = "Firstname";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(6, 35);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name";
            // 
            // grpActions
            // 
            this.grpActions.Controls.Add(this.cmdCancel);
            this.grpActions.Controls.Add(this.cmdSave);
            this.grpActions.Controls.Add(this.cmdEdit);
            this.grpActions.Location = new System.Drawing.Point(10, 296);
            this.grpActions.Name = "grpActions";
            this.grpActions.Size = new System.Drawing.Size(532, 55);
            this.grpActions.TabIndex = 2;
            this.grpActions.TabStop = false;
            this.grpActions.Text = "Actions";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(445, 20);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 2;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.UseVisualStyleBackColor = true;
            // 
            // cmdSave
            // 
            this.cmdSave.Enabled = false;
            this.cmdSave.Location = new System.Drawing.Point(235, 20);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(75, 23);
            this.cmdSave.TabIndex = 1;
            this.cmdSave.Text = "Save";
            this.cmdSave.UseVisualStyleBackColor = true;
            // 
            // cmdEdit
            // 
            this.cmdEdit.Location = new System.Drawing.Point(10, 20);
            this.cmdEdit.Name = "cmdEdit";
            this.cmdEdit.Size = new System.Drawing.Size(75, 23);
            this.cmdEdit.TabIndex = 0;
            this.cmdEdit.Text = "Edit";
            this.cmdEdit.UseVisualStyleBackColor = true;
            // 
            // FrmStudents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 363);
            this.Controls.Add(this.grpActions);
            this.Controls.Add(this.grpStudentName);
            this.Name = "FrmStudents";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "View - Student";
            this.Load += new System.EventHandler(this.FrmStudents_Load);
            this.grpStudentName.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picStudent)).EndInit();
            this.grpPersonalData.ResumeLayout(false);
            this.grpPersonalData.PerformLayout();
            this.grpActions.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpStudentName;
        private System.Windows.Forms.PictureBox picStudent;
        private System.Windows.Forms.GroupBox grpPersonalData;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.GroupBox grpActions;
        private System.Windows.Forms.Label lblFirstname;
        private System.Windows.Forms.TextBox txtFirstname;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.Button cmdEdit;
        private System.Windows.Forms.TextBox txtBirthday;
        private System.Windows.Forms.Label lblBirthday;
    }
}

