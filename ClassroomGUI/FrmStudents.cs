﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsClassroom
{
    public partial class FrmStudents : Form
    {
        public FrmStudents(string firstname, string lastname, string birthday, string picture)
        {
            InitializeComponent();
            grpStudentName.Text = firstname + " " + lastname;
            txtFirstname.Text = firstname;
            txtName.Text = lastname;
            txtBirthday.Text = birthday;
            Image image = Image.FromFile(picture);
            picStudent.Image = image;
        }

        private void FrmStudents_Load(object sender, EventArgs e)
        {
            
        }
    }
}
